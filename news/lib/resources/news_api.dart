import 'dart:convert';

import "package:http/http.dart" as http;
import '../models/news.dart';

Future<List<News>> fetchNews() async {
  final response = await http.get(Uri.parse('https://berita-indo-api.vercel.app/v1/cnn-news/'));
  if (response.statusCode == 200) {
      final jsonResponse = json.decode(response.body) as Map<String,dynamic>;
      final data = jsonResponse['data'] as List<dynamic>;
      return data
          .map((news) => new News.fromJson(news))
          .toList();
    } else {
      throw Exception('Failed to load News');
    }
}
