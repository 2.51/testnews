import 'package:flutter/material.dart';
import 'package:news/models/news.dart';
import 'package:news/resources/news_api.dart';
import 'package:news/ui/detailnews_screen.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<News>>(
      future: fetchNews(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return AlertDialog(
              title: Text("Error"),
              content: Text('An error has occured...'),
          );
        } else if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if(snapshot.hasData && snapshot.data != null) {
          return 
            GridView.builder(
              itemCount: snapshot.data!.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 5.0,
                mainAxisSpacing: 5.0,
              ),
              itemBuilder: (context, index) => 
              GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>  
                    DetailNews(id: snapshot.data![index].link)),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.network(
                      snapshot.data![index].image,
                      fit: BoxFit.cover,
                    ),
                    Text(
                      snapshot.data![index].title.length > 50 ?
                      snapshot.data![index].title.substring(0,50)+"..." : snapshot.data![index].title,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w600
                      ),
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            );
        }else{
          return Text('No Data Available');
        }
      },
    );
  }
}