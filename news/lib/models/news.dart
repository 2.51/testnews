class News {
  final String title;
  final String isoDate;
  final String link;
  final String content;
  final String image;

  News(
    {
      required this.title,
      required this.isoDate,
      required this.link,
      required this.content,
      required this.image
    }
  );

  factory News.fromJson(Map<String, dynamic> json) {
    return News(
        title: json['title'],
        isoDate: json['isoDate'],
        link: json['link'],
        content: json['contentSnippet'],
        image: json['image']['small']);
  }
}
