  </member>
    <member name="P:System.Threading.Tasks.ValueTask`1.IsCanceled">
      <summary>Gets a value that indicates whether this object represents a canceled operation.</summary>
      <returns>true if this object represents a canceled operation; otherwise, false.</returns>
    </member>
    <member name="P:System.Threading.Tasks.ValueTask`1.IsCompleted">
      <summary>Gets a value that indicates whether this object represents a completed operation.</summary>
      <returns>true if this object represents a completed operation; otherwise, false.</returns>
    </member>
    <member name="P:System.Threading.Tasks.ValueTask`1.IsCompletedSuccessfully">
      <summary>Gets a value that indicates whether this object represents a successfully completed operation.</summary>
      <returns>true if this object represents a successfully completed operation; otherwise, false.</returns>
    </member>
    <member name="P:System.Threading.Tasks.ValueTask`1.IsFaulted">
      <summary>Gets a value that indicates whether this object represents a failed operation.</summary>
      <returns>true if this object represents a failed operation; otherwise, false.</returns>
    </member>
    <member name="M:System.Threading.Tasks.ValueTask`1.op_Equality(System.Threading.Tasks.ValueTask{`0},System.Threading.Tasks.ValueTask{`0})">
      <summary>Compares two values for equality.</summary>
      <param name="left">The first value to compare.</param>
      <param name="right">The second value to compare.</param>
      <returns>true if the two <see cref="ValueTask{TResult}"/> values are equal; otherwise, false.</returns>
    </member>
    <member name="M:System.Threading.Tasks.ValueTask`1.op_Inequality(System.Threading.Tasks.ValueTask{`0},System.Threading.Tasks.ValueTask{`0})">
      <summary>Determines whether two <see cref="ValueTask{TResult}"/